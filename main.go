package main

import (
	"fmt"
	"math/rand"
	"os"
)

func main() {
	cards := deck{"Rupay", newCard()}
	// cards = append(cards, "Neo")
	// cards = newDeck(cards, "newCard1")
	// cards = newDeck(cards, "newCard2")
	fmt.Println("***************** Adding new Card from Input **************")
	args := os.Args[1]
	cards = newDeck(cards, args)

	// fmt.Println("***************** Printing All Cards **************")
	// cards.printCards()
	// fmt.Println("***************** End printing All Cards **************")

	// fmt.Println("***************** Counting Cards **************")
	// cardsCount := cards.countCards()
	// fmt.Println("Cards count : " + strconv.Itoa(cardsCount))
	// fmt.Println("***************** End counting cards **************")

	// fmt.Println("***************** Getting Cards by slice size **************")
	// s1, s2 := deal(cards, 2)
	// fmt.Println(s1, s2)
	// fmt.Println("***************** End getting cards **************")

	// str := cards.toString()
	// fmt.Println(str)
	// log.Print(str)

	// err := cards.saveToFile("deck.txt")
	// if err != nil {
	// 	log.Fatal(err)
	// }

	//str1 := newDeckFromFile("newDeck.txt")
	//str1.printCards()

	shfl := cards.shuffleDeck()
	shfl.printCards()

	source := rand.NewSource(10)
	r := rand.New(source)
	fmt.Println(r)
}

func newCard() string {
	card := "Master Card"
	return card
}
