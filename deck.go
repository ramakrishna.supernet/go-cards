package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"strings"
	"time"
)

type deck []string

func (d deck) printCards() {
	for i, card := range d {
		fmt.Println(i, card)
	}
}

func newDeck(d deck, n string) []string {
	d = append(d, n)
	return d
}

func (d deck) countCards() int {
	return len(d)
}

func deal(d deck, n int) (deck, deck) {
	return d[:n], d[n:]
}

// func writeDeckIntoFile(d deck) {
// 	//util.WriteFile("deck.txt", d)
// }

func (d deck) toString() string {
	return strings.Join([]string(d), ", ")
}

func (d deck) saveToFile(f string) error {
	return ioutil.WriteFile(f, []byte(d.toString()), 0777)
}

func newDeckFromFile(f string) deck {
	bs, err := ioutil.ReadFile(f)
	if err != nil {
		fmt.Println(err.Error())
	}
	strArr := strings.Split(string(bs), ",")
	return deck(strArr)
	//	return strArr
}

func (d deck) shuffleDeck() deck {
	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(d), func(i, j int) { d[i], d[j] = d[j], d[i] })
	return d
}
